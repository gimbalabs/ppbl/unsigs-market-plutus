{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}

import            Data.Aeson           as Json   (encode)
import            Data.ByteString.Lazy qualified as LB
import            System.Environment   (getArgs)
import            Prelude              hiding (id)
import            Data.String          (fromString)

import            Cardano.Api          (scriptDataToJson, ScriptDataJsonSchema(ScriptDataJsonDetailedSchema))
import            Cardano.Api.Shelley  (fromPlutusData)
import qualified  PlutusTx

import            Market.Types         (NFTSale(..))

-- This module is here to convert Haskell Data Types to JSON files, particularly used for NFTSale custom Datum Type.
-- To use this enter `cabal run datum-json <price> <seller> <id>`. (price in lovelace, seller as PubKeyHash)
-- You can use ( /testnet ) ./getPubKeyHash <wallet name> to get your wallet PubKeyHash

-- Constructs the JSON file for the datum, used as input to --tx-in-datum-file in cardano-cli

main :: IO ()
main = do
  [price', seller', id'] <- getArgs
  let price  = read price'
      seller = fromString seller'
      id     = fromString id'
      nftEx  = NFTSale seller price id
  writeData ("testnet/datum-"  ++ id' ++ ".json") nftEx
  putStrLn "Done"


-- Datum also needs to be passed when sending the token to the script (aka putting for sale)
-- When doing this, the datum needs to be hashed, use cardano-cli transaction hash-script-data ...


writeData :: PlutusTx.ToData a => FilePath -> a -> IO ()
writeData file isData = do
  print file
  LB.writeFile file (toJsonString isData)

toJsonString :: PlutusTx.ToData a => a -> LB.ByteString
toJsonString =
  Json.encode
    . scriptDataToJson ScriptDataJsonDetailedSchema
    . fromPlutusData
    . PlutusTx.toData
