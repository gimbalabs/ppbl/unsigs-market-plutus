{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE LambdaCase         #-}
{-# LANGUAGE RankNTypes         #-}
{-# LANGUAGE TypeApplications   #-}
{-# LANGUAGE TypeFamilies       #-}
{-# LANGUAGE TypeOperators      #-}
{-# LANGUAGE OverloadedStrings  #-}

module Main(main) where

import           Control.Monad                       (void)
import           Control.Monad.Freer                 (interpret)
import           Control.Monad.IO.Class              (MonadIO (..))
import           Data.Aeson                          (FromJSON (..), ToJSON (..), genericToJSON, genericParseJSON
                                                     , defaultOptions, Options(..))
import           Data.Default                        (def)
import qualified Data.OpenApi                        as OpenApi
import           Data.Semigroup                      as Semigroup
import           Prettyprinter                       (Pretty (..), viaShow)
import           GHC.Generics                        (Generic)
import           Plutus.Contract                     (Empty, Contract (..), submitTx, awaitTxConfirmed, tell, logInfo, ownPubKeyHash)
import           Plutus.Contracts.Currency           as Currency
import           Plutus.PAB.Effects.Contract.Builtin (Builtin, SomeBuiltin (..), BuiltinHandler(contractHandler))
import qualified Plutus.PAB.Effects.Contract.Builtin as Builtin
import           Plutus.PAB.Simulator                (SimulatorEffectHandlers)
import qualified Plutus.PAB.Simulator                as Simulator
import qualified Plutus.PAB.Webserver.Server         as PAB.Server
import           Ledger.Value                        as Value
import           Ledger                              (getCardanoTxId)
import           Ledger.Constraints                  (mustPayToPubKey)
import           Wallet.Emulator.Types               (walletPubKeyHash)

import           Market.Types
import           Market.Offchain 
import           Utility


main :: IO ()
main = void $ Simulator.runSimulationWith handlers $ do
    Simulator.logString @(Builtin StarterContracts) "Starting unsigs-market PAB webserver on port 8080. Press enter to exit."
    shutdown <- PAB.Server.startServerDebug
    initT    <- Simulator.activateContract (wallet 5) Init
    _        <- Simulator.waitUntilFinished initT
    void $ liftIO getLine

    Simulator.logString @(Builtin StarterContracts) "Balances at the end of the simulation"
    b <- Simulator.currentBalances
    Simulator.logBalances @(Builtin StarterContracts) b

    shutdown


data StarterContracts =
    MarketContract | Init 
    deriving (Eq, Ord, Show, Generic)
    deriving anyclass OpenApi.ToSchema

instance ToJSON StarterContracts where
  toJSON = genericToJSON defaultOptions {
             tagSingleConstructors = True }
instance FromJSON StarterContracts where
  parseJSON = genericParseJSON defaultOptions {
             tagSingleConstructors = True }

instance Pretty StarterContracts where
    pretty = viaShow

instance Builtin.HasDefinitions StarterContracts where
    getDefinitions = [MarketContract, Init]
    getSchema =  \case
        MarketContract -> Builtin.endpointsToSchemas @Market.Types.SaleSchema
        Init           -> Builtin.endpointsToSchemas @Empty
    getContract = \case
        MarketContract -> SomeBuiltin Market.Offchain.endpoints
        Init           -> SomeBuiltin initContract

handlers :: SimulatorEffectHandlers (Builtin StarterContracts)
handlers =
    Simulator.mkSimulatorHandlers def def
    $ interpret (contractHandler Builtin.handleBuiltin)

initContract :: Contract (Maybe (Semigroup.Last Currency.OneShotCurrency)) Currency.CurrencySchema Currency.CurrencyError ()
initContract = do
    ownPkh <- ownPubKeyHash
    cur   <- Currency.mintContract ownPkh [(tn, 1) | tn <- tns]
    let cs  = Currency.currencySymbol cur
        v   = mconcat [Value.singleton cs tn 1 | tn <- tns]
        pkh = walletPubKeyHash (wallet 3)
    tx <- submitTx $ mustPayToPubKey pkh v
    awaitTxConfirmed $ getCardanoTxId tx
    logInfo @String $ "currencySymbol: " <> show cs
    tell $ Just $ Semigroup.Last cur



tns :: [TokenName] 
tns = ["unsig0", "unsig1"] 


