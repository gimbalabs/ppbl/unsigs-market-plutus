#!/bin/bash
printf "\nUnsigs NFT marketplace.\n"

SellerW_ID=$(echo "c30efb78b4e272685c1f9f0c93787fd4b6743154")
BuyerW_ID=$(echo "5f5a4f5f465580a5500b9a9cede7f4e014a37ea8")
BadUserW_ID=$(echo "d3eddd0d37989746b029a0e050386bc425363901")

printf "\nThis demo will show a NFT marketplace\n"
printf "The tokens (unsig0, unsig1) are already minted. You can see the CurrrencySymbol in the logs.\n"
sleep 1
printf "\nWe will use 3 wallets. One for the seller (walletId: '$SellerW_ID'), another one for the buyer (walletId: '$BuyerW_ID')\n" 
printf "and the last, is a bad user (walletId: '$BadUserW_ID') which we use to show that only the seller can update the sale.\n"
printf "There are also two other wallets which represent the DAO and the artist of the NFT. We don't use them, they only get the fees.\n\n"

read -n1 -r -p "Press any key to continue..." key
printf "\n"

export SellerW_IID=$(curl -s -H "Content-Type: application/json" -X POST -d '{"caID": "MarketContract", "caWallet":{"getWalletId": "'$SellerW_ID'"}}' http://localhost:9080/api/contract/activate | jq .unContractInstanceId | tr -d '"')
export BuyerW_IID=$(curl -s -H "Content-Type: application/json" -X POST -d '{"caID": "MarketContract", "caWallet":{"getWalletId": "'$BuyerW_ID'"}}' http://localhost:9080/api/contract/activate | jq .unContractInstanceId | tr -d '"')
export BadUserW_IID=$(curl -s -H "Content-Type: application/json" -X POST -d '{"caID": "MarketContract", "caWallet":{"getWalletId": "'$BadUserW_ID'"}}' http://localhost:9080/api/contract/activate | jq .unContractInstanceId | tr -d '"')
sleep 1

#startSale
curl -H "Content-Type: application/json" -X POST -d '{"price":1000000000, "id":"30"}' http://localhost:9080/api/contract/instance/$SellerW_IID/endpoint/start
sleep 1
printf "\n\nNow we offered unsig0 for sale. You can see the transaction in the log.\n"
read -n1 -r -p "Press any key to continue..." key 

#buy
printf "\n"
curl -H "Content-Type: application/json" -X POST -d '{"price":1000000000, "id":"30"}' http://localhost:9080/api/contract/instance/$BuyerW_IID/endpoint/buy
sleep 1 
printf "\n\nThe buyer bought the NFT. The price of the NFT was 1000000000. The DAO and he artist each get 1250000 (1,25 %) as fee. The remainder (97500000) gets to the seller.\n"
read -n1 -r -p "Press any key to continue..." key

#update
printf "\n"
curl -H "Content-Type: application/json" -X POST -d '{"price":1000000000, "id":"31"}' http://localhost:9080/api/contract/instance/$SellerW_IID/endpoint/start
sleep 1
printf "\n\nunsig1 is now for sale.\n"
sleep 1

printf "\nNow we want to update the price of the NFT.\n"
sleep 2
printf "\n"
curl -H "Content-Type: application/json" -X POST -d '[{"price":1000000000, "id":"31"}, 1110000000]' http://localhost:9080/api/contract/instance/$BadUserW_IID/endpoint/update
sleep 1 
printf "\n\nGood try but that was the wrong user. You can see an error message in the logs.\n"
read -n1 -r -p "Press any key to continue..." key

printf "\n"
curl -H "Content-Type: application/json" -X POST -d '[{"price":1000000000, "id":"31"}, 1110000000]' http://localhost:9080/api/contract/instance/$SellerW_IID/endpoint/update
sleep 1
printf "\n\nThis time the owner of the NFT updated the price and it worked.\n"
read -n1 -r -p "Press any key to continue..." key 

#close
printf "\n"
curl -H "Content-Type: application/json" -X POST -d '{"price":1110000000, "id":"31"}' http://localhost:9080/api/contract/instance/$SellerW_IID/endpoint/close
sleep 1 
printf "\n\nNobody bought the NFT therefore we closed the sale.\n"
read -n1 -r -p "Press any key to continue..." key 

printf "\n\nFinally we End the simulation, change to the pab terminal and press ENTER. You can see the balance of each wallet at the End.\n"
printf "\nThe first 3 walles have the same balance as at the beginning because we didn't used them."
printf "\nWallet 4 bought unsig0 and payed 1000000000 + transaction fee." 
printf "\nThe next two wallets are the DAO and artist. Both got 12500000 as fee."
printf "\nWallet 9 is the seller and got 975000000 - transaction fee."
printf "\nThe last wallet we used only for minting the NFTs which costs only transaction fees.\n\n"

sleep 4

