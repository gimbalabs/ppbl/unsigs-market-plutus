#example input: ./buyToken.sh 00005 wallet1 10000000

source getTxFunc.sh

getInputTx $2
FROM_UTXO=${SELECTED_UTXO}
FROM_WALLET_NAME=${SELECTED_WALLET_NAME}
FROM_WALLET_ADDRESS=${SELECTED_WALLET_ADDR}


getInputTx ../market-testnet
CON_FROM_UTXO=${SELECTED_UTXO}
CON_FROM_WALLET_NAME=${SELECTED_WALLET_NAME}
CON_FROM_WALLET_ADDRESS=${SELECTED_WALLET_ADDR}

FEE=$(echo " $3 * 0.0125" | bc)
FEE=${FEE%.*}
if [ $FEE -lt 2000000 ] 

then FEE=2000000

fi

hexTn=$(cat datum-$1.json | jq -r '.fields[2].bytes') 

cardano-cli transaction build \
    --alonzo-era \
    $Testnet \
    --tx-in ${FROM_UTXO} \
    --tx-in ${CON_FROM_UTXO} \
    --tx-in-script-file market-testnet.plutus \
    --tx-in-datum-file datum-$1.json \
    --tx-in-redeemer-file buy.json \
    --required-signer wallets/${FROM_WALLET_NAME}.skey \
    --tx-in-collateral ${FROM_UTXO} \
    --tx-out "${FROM_WALLET_ADDRESS} + 2000000 + 1 1e82bbd44f7bd555a8bcc829bd4f27056e86412fbb549efdbf78f42d.756e736967$hexTn" \
    --tx-out $(cat wallets/dao.addr)+$FEE \
    --tx-out $(cat wallets/artist.addr)+$FEE \
    --tx-out ${FROM_WALLET_ADDRESS}+$3 \
    --change-address ${FROM_WALLET_ADDRESS} \
    --protocol-params-file protocol.json \
    --out-file tx/buy$1.draft 

cardano-cli transaction sign \
    --tx-body-file tx/buy$1.draft \
    --signing-key-file wallets/${FROM_WALLET_NAME}.skey \
    $Testnet \
    --out-file tx/buy$1.signed 

cardano-cli transaction submit --tx-file tx/buy$1.signed $Testnet  
