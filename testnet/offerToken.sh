#example Input: ./offerToken.sh 00005 wallet1

source getTxFunc.sh

getInputTx $2
FROM_UTXO=${SELECTED_UTXO}
FROM_WALLET_NAME=${SELECTED_WALLET_NAME}
FROM_WALLET_ADDRESS=${SELECTED_WALLET_ADDR}

hexTn=$(cat datum-$1.json | jq -r '.fields[2].bytes') 

cardano-cli transaction hash-script-data --script-data-file datum-$1.json > dhash$1

cardano-cli transaction build \
    --alonzo-era \
    $Testnet \
    --tx-in ${FROM_UTXO} \
    --tx-out "$(cat market-testnet.addr) + 2000000 lovelace + 1 1e82bbd44f7bd555a8bcc829bd4f27056e86412fbb549efdbf78f42d.756e736967$hexTn" \
    --tx-out-datum-hash $(cat dhash$1) \
    --change-address ${FROM_WALLET_ADDRESS} \
    --protocol-params-file protocol.json \
    --out-file tx/tx$1.draft 


cardano-cli transaction sign \
    --tx-body-file tx/tx$1.draft \
    --signing-key-file wallets/${FROM_WALLET_NAME}.skey \
    $Testnet \
    --out-file tx/tx$1.signed 

cardano-cli transaction submit --tx-file tx/tx$1.signed $Testnet 

