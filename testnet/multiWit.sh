


cardano-cli transaction build --tx-in   bccab13f890bf2b3f1d19aebd393633ea03c9b6463dedef8f5ff904f875ee313#1 --tx-in bccab13f890bf2b3f1d19aebd393633ea03c9b6463dedef8f5ff904f875ee313#0 --tx-out "$(cat wallets/wallet1.addr) +5000000+1 1e82bbd44f7bd555a8bcc829bd4f27056e86412fbb549efdbf78f42d.756e7369673030303138"   $Testnet --out-file tx/mtx.draft --change-address $(cat wallets/wallet1.addr) --alonzo-era

cardano-cli transaction sign --tx-body-file tx/mtx.draft --signing-key-file wallets/wallet1.skey --out-file tx/mtx.signed 

cardano-cli transaction submit --tx-file tx/mtx.signed $Testnet
