{-# LANGUAGE OverloadedStrings #-}

module Utility
       ( wallet
       , daopkh
       , artistpkh
       , feePayment
       , polUns
       , preUns
       --testnet
       , testnetFeePayment
       --mainnet 
       , mainnetFeePayment
       ) where


import           Plutus.V1.Ledger.Crypto         (PubKeyHash)
import           Plutus.V1.Ledger.Value          (CurrencySymbol(..))
import           Wallet.Emulator.Types           (WalletNumber (..), fromWalletNumber, walletPubKeyHash, Wallet (..))
import           PlutusTx.Prelude                ((.), BuiltinByteString)
import           Prelude                         hiding ((.))

import           Market.Types                    (FeePayment (..))


--mainnet
mainnetFeePayment :: FeePayment
mainnetFeePayment = FeePayment
                { artistPkh   = "1a1a38b3e1b7d6a304ef1055f7602ac4572007be647fb64f9642083d"
                , daoPkh      = "663ba191d77ad725322e04492abe5dd09a49d57a0a2635f06f542fcc"
                , policyUnsigs = "0e14267a8020229adc0184dd25fa3174c3f7d6caadcb4425c70e7c04"
                , prefixUnsigs = "unsig"
                }


--local
wallet :: Integer -> Wallet
wallet = fromWalletNumber . WalletNumber

daopkh :: PubKeyHash
daopkh = walletPubKeyHash $ wallet 1

artistpkh :: PubKeyHash
artistpkh = walletPubKeyHash $ wallet 2

polUns :: CurrencySymbol
polUns = "2b23e423a5ab5de9bed02187ee2e240e8ea0a21bd3dbe2c498ec75d0"

preUns :: BuiltinByteString 
preUns = "unsig"

feePayment :: FeePayment
feePayment = FeePayment 
                { artistPkh   = artistpkh
                , daoPkh      = daopkh
                , policyUnsigs = polUns
                , prefixUnsigs = preUns 
                }

--testnet
testnetDaoPkh :: PubKeyHash
testnetDaoPkh = "3795730786fe4960e5d6750ac3e5e91350181ab874828d79ecaefc91"

testnetArtistPkh :: PubKeyHash
testnetArtistPkh = "0b83b3a70145fcb7936491e8ee22bcdb8cf235be855c2f64eb016665"

testnetFeePayment :: FeePayment 
testnetFeePayment = FeePayment 
                { artistPkh   = testnetArtistPkh
                , daoPkh      = testnetDaoPkh
                , policyUnsigs = "1e82bbd44f7bd555a8bcc829bd4f27056e86412fbb549efdbf78f42d"
                , prefixUnsigs = "unsig"
                }  

