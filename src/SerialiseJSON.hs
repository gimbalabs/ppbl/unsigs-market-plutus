{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}

module SerialiseJSON (testR, testD) where

import              Data.Aeson                     as Json ( encode )
import              Data.ByteString.Lazy qualified as LB


import              Cardano.Api                    (scriptDataToJson, ScriptDataJsonSchema(ScriptDataJsonDetailedSchema))
import              Cardano.Api.Shelley            (fromPlutusData)
import qualified    PlutusTx

import              Market.Types                   (SaleAction(..), NFTSale(..))

-- This module is here to convert Haskell Data Types to JSON files, particularly used for Redeemer and Datum
-- To use this enter `cabal repl` ; `:l src/SerialiseJSON.hs` ; `testR` or `testD`


-- Constructs the JSON file for the Buy Redeemer constructor, used as input to --tx-in-redeemer-file
testR :: IO ()
testR = do 
  writeData "update.json" Update
  putStrLn "Done"


nftEx :: NFTSale
nftEx = NFTSale
    { nPrice    = 1
    , nSeller   = "0b83b3a70145fcb7936491e8ee22bcdb8cf235be855c2f64eb016665"
    , unsigsId    = "0"
    }  -- This is an example to fill with real data
 
-- Constructs the JSON file for the nftEx datum, used as input to --tx-in-datum-file
testD :: IO ()
testD = do
  writeData "datum.json" nftEx
  putStrLn "Done"
-- Datum also needs to be passed when sending the token to the script (aka putting for sale)
  
writeData :: PlutusTx.ToData a => FilePath -> a -> IO ()
writeData file isData = LB.writeFile file (toJsonString isData)

toJsonString :: PlutusTx.ToData a => a -> LB.ByteString
toJsonString =
  Json.encode
    . scriptDataToJson ScriptDataJsonDetailedSchema
    . fromPlutusData
    . PlutusTx.toData
