{-# LANGUAGE NumericUnderscores         #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeApplications           #-}

module Market.Offchain (endpoints) where

import qualified Data.Map                  as Map
import           Data.Monoid               as Mnd  ((<>))
import           Control.Monad             (void, forever)
import           Data.Text                 (Text)
import           Prelude                   (String, fromIntegral, ceiling, Float, (*), show)
import           Plutus.Contract           as Contract
                                           (AsContractError, logError, logInfo, awaitTxConfirmed, endpoint, submitTxConstraintsWith, utxosAt,
                                           utxosTxOutTxAt, handleError, select, Contract, Promise(awaitPromise), ownPubKeyHash)
import qualified PlutusTx
import           PlutusTx.Prelude          as Plutus
                                           (return, Bool, Maybe(..), Eq((==)), (<$>), ($), Integer, BuiltinByteString)
import           Ledger                    (scriptAddress, pubKeyHashAddress, Redeemer(Redeemer), TxOut(txOutValue), TxOutRef, ChainIndexTxOut,
                                           toTxOut, getCardanoTxId)
import           Ledger.Constraints        as Constraints
                                           (otherScript, typedValidatorLookups, unspentOutputs, mustPayToPubKey, mustPayToTheScript, mustSpendScriptOutput)
import           Ledger.Value              as Value (singleton, valueOf )
import qualified Plutus.V1.Ledger.Ada      as Ada   (lovelaceValueOf)
import           Plutus.V1.Ledger.Value    (TokenName (..))
import           Plutus.ChainIndex.Tx      (ChainIndexTx(_citxData))

import           Market.Types              (NFTSale(..), SaleAction(..), SaleSchema, TradeParams (..), FeePayment(..))
import           Market.Onchain            ( Sale, typedBuyValidator, buyValidator, nftDatum )
import           Utility                   (feePayment)


startSale :: TradeParams -> Contract w SaleSchema Text ()
startSale tp = do
    pkh <- Contract.ownPubKeyHash
    utxos <- utxosAt (pubKeyHashAddress pkh)
    let val     = Value.singleton (policyUnsigs feePayment) (TokenName (prefixUnsigs feePayment <> id tp)) 1
        dat     = NFTSale { nSeller = pkh, nPrice = price tp, unsigsId = id tp }
        lookups = Constraints.unspentOutputs utxos                         <>
                  Constraints.typedValidatorLookups (typedBuyValidator feePayment)
        tx      = Constraints.mustPayToTheScript dat val
    ledgerTx <- submitTxConstraintsWith @Sale lookups tx
    void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
    Contract.logInfo @String "startSale transaction confirmed"


buy :: TradeParams -> Contract w SaleSchema Text ()
buy tp = do   
    pkh <- Contract.ownPubKeyHash
    sale <- findSale (price tp, id tp)
    case sale of
        Nothing -> Contract.logError @String "No sale found"
        Just (oref, o, nfts) -> do
            let r       = Redeemer $ PlutusTx.toBuiltinData Buy
                val     = Value.singleton (policyUnsigs feePayment) (TokenName (prefixUnsigs feePayment <> id tp)) 1 <> Ada.lovelaceValueOf 1724100
                valAdaS = Ada.lovelaceValueOf (ceiling (0.975 Prelude.* fromIntegral (nPrice nfts) :: Float))
                valAdaF = Ada.lovelaceValueOf (ceiling (0.0125 Prelude.* fromIntegral (nPrice nfts) :: Float))
                lookups = Constraints.typedValidatorLookups (typedBuyValidator feePayment) <>
                          Constraints.unspentOutputs (Map.singleton oref o)   <>
                          Constraints.otherScript (buyValidator feePayment)
                tx      = Constraints.mustSpendScriptOutput oref r          <>
                          Constraints.mustPayToPubKey pkh val               <>
                          Constraints.mustPayToPubKey (nSeller nfts) valAdaS <>
                          Constraints.mustPayToPubKey (daoPkh feePayment) valAdaF <>
                          Constraints.mustPayToPubKey (artistPkh feePayment) valAdaF 
            ledgerTx <- submitTxConstraintsWith lookups tx
            void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
            Contract.logInfo @String "buy transaction confirmed"


update :: (TradeParams, Integer) -> Contract w SaleSchema Text ()
update (tp, newprice) = do
    sale <- findSale (price tp, id tp)
    case sale of
        Nothing -> Contract.logError @String "No sale found"
        Just (oref, o, nfts) -> do
            let r       = Redeemer $ PlutusTx.toBuiltinData Update
                val     = Value.singleton (policyUnsigs feePayment) (TokenName (prefixUnsigs feePayment <> id tp)) 1
                dat     = nfts { nPrice = newprice }
                lookups = Constraints.typedValidatorLookups (typedBuyValidator feePayment) <>
                          Constraints.otherScript (buyValidator feePayment)                <>
                          Constraints.unspentOutputs (Map.singleton oref o)
                tx      = Constraints.mustSpendScriptOutput oref r <>
                          Constraints.mustPayToTheScript dat val   
            ledgerTx <- submitTxConstraintsWith lookups tx
            void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
            Contract.logInfo @String $ "Price updated: " <> show newprice


close :: TradeParams -> Contract w SaleSchema Text ()
close tp = do
    sale <- findSale (price tp, id tp)
    case sale of
        Nothing -> Contract.logError @String "No sale found"
        Just (oref, o, nfts) -> do
            let r       = Redeemer $ PlutusTx.toBuiltinData Close
                val     = Value.singleton (policyUnsigs feePayment) (TokenName (prefixUnsigs feePayment <> id tp)) 1 <> Ada.lovelaceValueOf 1724100
                lookups = Constraints.typedValidatorLookups (typedBuyValidator feePayment) <>
                          Constraints.otherScript (buyValidator feePayment)                <>
                          Constraints.unspentOutputs (Map.singleton oref o)
                tx      = Constraints.mustSpendScriptOutput oref r      <>
                          Constraints.mustPayToPubKey (nSeller nfts) val
            ledgerTx <- submitTxConstraintsWith lookups tx
            void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
            Contract.logInfo @String "close transaction confirmed"


findSale :: (AsContractError e) => (Integer, BuiltinByteString) -> Contract w SaleSchema e (Maybe (TxOutRef, ChainIndexTxOut, NFTSale))
findSale (_, b) = do
    utxos <- Map.filter f <$> utxosTxOutTxAt (scriptAddress $ buyValidator feePayment)
    return $ case Map.toList utxos of
        [(oref, (o, citx))] -> do
            nftSale <- nftDatum (toTxOut o) $ \dh -> Map.lookup dh $ _citxData citx
            Just (oref, o, nftSale)
        _           -> Nothing

  where
    f :: (ChainIndexTxOut, Plutus.ChainIndex.Tx.ChainIndexTx) -> Bool
    f (o, _) = valueOf (txOutValue $ toTxOut o) (policyUnsigs feePayment) (TokenName (prefixUnsigs feePayment <> b)) == 1


endpoints :: Contract () SaleSchema Text ()
endpoints = forever
          $ handleError logError
          $ awaitPromise
          $ start' `select` buy' `select` close' `select` update'
  where
    start'  = endpoint @"start"  $ \nfts      -> startSale nfts
    buy'    = endpoint @"buy"    $ \nfts      -> buy nfts
    close'  = endpoint @"close"  $ \nfts      -> close nfts
    update' = endpoint @"update" $ \(nfts, x) -> update (nfts, x)
