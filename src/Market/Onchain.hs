{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE DerivingStrategies         #-}

{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}

module Market.Onchain
    ( apiBuyScript
    , buyScriptAsShortBs
    , typedBuyValidator
    , Sale
    , buyValidator
    , nftDatum
    ) where

import qualified Data.ByteString.Lazy     as LB
import qualified Data.ByteString.Short    as SBS
import           Codec.Serialise          ( serialise )

import           Cardano.Api.Shelley      (PlutusScript (..), PlutusScriptV1)
import qualified PlutusTx
import           PlutusTx.Prelude         as Plutus
                                          (Bool(..), Eq((==)), (.), (&&), traceIfFalse, Integer, Maybe(..), (>=), fromInteger, (*), (%), (<>), length)
import           Ledger                   (TokenName, PubKeyHash(..), CurrencySymbol, DatumHash, Datum(..), txOutDatum, txSignedBy, ScriptContext(scriptContextTxInfo),
                                          TxInfo, Validator, TxOut, txInfoSignatories, unValidatorScript, valuePaidTo, findDatum, txInfoOutputs, txOutValue, getContinuingOutputs, valueSpent)
import qualified Ledger.Typed.Scripts     as Scripts
import qualified Plutus.V1.Ledger.Scripts as Plutus
import           Ledger.Value             as Value ( valueOf)
import           Plutus.V1.Ledger.Value   (TokenName (..), flattenValue, Value)
import qualified Plutus.V1.Ledger.Ada     as Ada (fromValue, Ada (getLovelace))

import           Market.Types             (NFTSale(..), SaleAction(..), FeePayment(..))


{-# INLINABLE nftDatum #-}
nftDatum :: TxOut -> (DatumHash -> Maybe Datum) -> Maybe NFTSale
nftDatum o f = do
    dh <- txOutDatum o
    Datum d <- f dh
    PlutusTx.fromBuiltinData d

{-# INLINABLE mkBuyValidator #-}
mkBuyValidator :: FeePayment -> NFTSale -> SaleAction -> ScriptContext -> Bool
mkBuyValidator fees nfts r ctx =
    case r of
        Buy    -> traceIfFalse "NFT not sent to buyer" checkNFTOut    &&
                  traceIfFalse "Seller not paid"       checkSellerOut &&
                  traceIfFalse "Fee not paid"          checkFee       &&
                  traceIfFalse "Only one unsig"        correctOutput 
        Update -> traceIfFalse "No rights to perform this action" checkUser  &&
                  traceIfFalse "Modified datum other than price"  checkDatum &&
                  traceIfFalse "NFT left the script"              checkContinuingNFT
        Close  -> traceIfFalse "No rights to perform this action" checkUser &&
                  traceIfFalse "Close output invalid"             checkCloseOut
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    outVal :: Value
    outVal = valueSpent info

    tn :: TokenName
    tn = TokenName (prefixUnsigs fees <> unsigsId nfts)

    cs :: CurrencySymbol
    cs = policyUnsigs fees

    seller :: PubKeyHash
    seller = nSeller nfts

    sig :: PubKeyHash
    sig = case txInfoSignatories info of
            [pubKeyHash] -> pubKeyHash

    price :: Integer
    price = nPrice nfts

    getSaleDatum :: Maybe NFTSale
    getSaleDatum = let os = [ o | o <- txInfoOutputs info, valueOf (txOutValue o) cs tn == 1 ] in
                  case os of
                    [o] -> nftDatum o (`findDatum` info)
                    _   -> Nothing
    
    checkNFTOut :: Bool
    checkNFTOut = valueOf (valuePaidTo info sig) cs tn == 1
    
    checkSellerOut :: Bool
    checkSellerOut = fromInteger (Ada.getLovelace (Ada.fromValue (valuePaidTo info seller))) >= 9750 % 10000 * fromInteger price

    checkFee :: Bool
    checkFee = fromInteger (Ada.getLovelace (Ada.fromValue (valuePaidTo info (artistPkh fees)))) >= 125 % 10000 * fromInteger price &&
               fromInteger (Ada.getLovelace (Ada.fromValue (valuePaidTo info (daoPkh fees)))) >= 125 % 10000 * fromInteger price 

    checkUser :: Bool
    checkUser = txSignedBy info seller

    checkDatum :: Bool
    checkDatum = case getSaleDatum of
      Nothing -> False
      Just ns -> nSeller   ns == nSeller   nfts &&
                 unsigsId ns == unsigsId nfts 

    correctOutput :: Bool 
    correctOutput = 
        let 
            ov = [ vcs
                 | (vcs, _, _) <-  flattenValue outVal
                 , vcs == cs
                 ] 
        in
          length ov == 1

    checkContinuingNFT :: Bool
    checkContinuingNFT = let cos = [ co | co <- getContinuingOutputs ctx, valueOf (txOutValue co) cs tn == 1 ] in
        case cos of
            [_] -> True
            _   -> False

    checkCloseOut :: Bool
    checkCloseOut = valueOf (valuePaidTo info seller) cs tn == 1


data Sale
instance Scripts.ValidatorTypes Sale where
    type instance DatumType Sale    = NFTSale
    type instance RedeemerType Sale = SaleAction


typedBuyValidator :: FeePayment -> Scripts.TypedValidator Sale
typedBuyValidator fees = Scripts.mkTypedValidator @Sale
    ($$(PlutusTx.compile [|| mkBuyValidator ||]) `PlutusTx.applyCode` PlutusTx.liftCode fees)
    $$(PlutusTx.compile [|| wrap ||])
  where
    wrap = Scripts.wrapValidator @NFTSale @SaleAction


buyValidator :: FeePayment -> Validator
buyValidator = Scripts.validatorScript . typedBuyValidator

buyScript :: FeePayment -> Plutus.Script
buyScript = Ledger.unValidatorScript . buyValidator

buyScriptAsShortBs :: FeePayment -> SBS.ShortByteString
buyScriptAsShortBs = SBS.toShort . LB.toStrict . serialise . buyScript

apiBuyScript :: FeePayment -> PlutusScript PlutusScriptV1
apiBuyScript = PlutusScriptSerialised . buyScriptAsShortBs


