{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}

module Market.Trace
    ( test
    , startTest
    , buyTest
    , closeTest
    , updateTest
    ) where


import           Plutus.Trace.Emulator          as Emulator
                                                (activateContractWallet, waitNSlots, runEmulatorTraceIO', callEndpoint, EmulatorConfig(..))
import           Control.Monad                  (void)
import           PlutusTx.Prelude               as Plutus ( ($), (<>), Either(..), BuiltinByteString )
import           Ledger.Value                   as Value (singleton)
import qualified Data.Map                       as Map
import qualified Ledger.Ada                     as Ada
import           Prelude                        (IO)
import           Data.Default                   (def)
import           Plutus.V1.Ledger.Value         (Value (..))
import           Wallet.Emulator.Wallet         (Wallet (..))

import           Utility                        (wallet, polUns)
import           Market.Offchain                (endpoints)
import           Market.Types                   (TradeParams (..))

nftEx1 :: BuiltinByteString -> TradeParams
nftEx1 bid = TradeParams
    { price = 1_000_000
    , id    = bid
    } 

dist :: Map.Map Wallet Value 
dist = Map.fromList [ (wallet 1, Ada.lovelaceValueOf 100_000_000)
                            , (wallet 2, Ada.lovelaceValueOf 100_000_000)
                            , (wallet 3, Ada.lovelaceValueOf 100_000_000)
                            , (wallet 4, Ada.lovelaceValueOf 100_000_000
                                      <> Value.singleton (polUns) "unsig0" 1
                                      <> Value.singleton (polUns) "unsig1" 1
                                      <> Value.singleton (polUns) "unsig2" 1)
                            , (wallet 5, Ada.lovelaceValueOf 100_000_000)
                            ]
     
emCfg :: EmulatorConfig
emCfg = EmulatorConfig (Left dist) def def 

startTest :: IO ()
startTest = 
    runEmulatorTraceIO' def emCfg $ do 
        h4 <- activateContractWallet (wallet 4) endpoints
        void $ Emulator.waitNSlots 1 
        callEndpoint @"start" h4 $ nftEx1 "1"
        void $ Emulator.waitNSlots 1

buyTest :: IO ()
buyTest =  
    runEmulatorTraceIO' def emCfg $ do 
        h4 <- activateContractWallet (wallet 4) endpoints
        h5 <- activateContractWallet (wallet 5) endpoints
        void $ Emulator.waitNSlots 1 
        callEndpoint @"start" h4 $ nftEx1 "1"
        void $ Emulator.waitNSlots 1 
        callEndpoint @"buy" h5 $ nftEx1 "1"
        void $ Emulator.waitNSlots 1

updateTest :: IO ()
updateTest =  
    runEmulatorTraceIO' def emCfg $ do 
        h4 <- activateContractWallet (wallet 4) endpoints
        void $ Emulator.waitNSlots 1 
        callEndpoint @"start" h4 $ nftEx1 "1"
        void $ Emulator.waitNSlots 1 
        callEndpoint @"update" h4 $ (nftEx1 "1", 11_000_000)
        void $ Emulator.waitNSlots 1

closeTest :: IO ()
closeTest =  
    runEmulatorTraceIO' def emCfg $ do 
        h4 <- activateContractWallet (wallet 4) endpoints
        void $ Emulator.waitNSlots 1
        callEndpoint @"start" h4 $ nftEx1 "1"
        void $ Emulator.waitNSlots 1
        callEndpoint @"close" h4 $ nftEx1 "1"
        void $ Emulator.waitNSlots 1

test :: IO ()
test =
    runEmulatorTraceIO' def emCfg $ do
        h4 <- activateContractWallet (wallet 4) endpoints
        h5 <- activateContractWallet (wallet 5) endpoints
        void $ Emulator.waitNSlots 1
        callEndpoint @"start" h4 $ nftEx1 "0"
        void $ Emulator.waitNSlots 1
        callEndpoint @"start" h4 $ nftEx1 "1"
        void $ Emulator.waitNSlots 1 
        callEndpoint @"start" h4 $ nftEx1 "2"
        void $ Emulator.waitNSlots 1
        callEndpoint @"buy" h5 $ nftEx1 "0" 
        callEndpoint @"update" h4 $ (nftEx1 "1", 11_000_000)
        void $ Emulator.waitNSlots 1
        callEndpoint @"buy" h5 $ nftEx1 "2" 
        void $ Emulator.waitNSlots 1
    
