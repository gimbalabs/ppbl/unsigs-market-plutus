# Unsigs market plutus 

This project shows the plutus code for an NFT marketplace, built by [gimbalabs](https://gimbalabs.com/gimbalgrid) and for [unsigs](https://www.unsigs.com/)

## Setting up

Set up the [plutus platform](https://github.com/input-output-hk/plutus-apps).

## Test locally 

* run `cabal build`
* run `cabal repl`
* run `import Market.Trace`
* now you can run a trace for every endpoint (startTest, buyTest, closeTest, updateTest)


## Test in local PAB

* run `cabal build`
* run `cabal exec unsigs-market-pab`
* open a new terminal and run `cd pab`
* run `./runDemo.sh`


## Testnet (cardano-cli)

#### offer NFT

* Install and run a [`cardano-node`](https://github.com/input-output-hk/cardano-node) on the testnet, you'll now have a node.socket file. Put the path to this file in the env variable `CARDANO_NODE_SOCKET_PATH`
* run `Testnet=--testnet testnet-magic 1097911063`
* save your vkey, skey, addr files in testnet/wallets/ 
* run `./getPubKeyHash wallet` where wallet is the name of the key-files (/testnet) 
* build the project with `cabal build` (/unsigs-market-plutus)
* run `cabal run market-plutus`
* copy the resulting `market.plutus` file in the testnet folder 
* run `cabal run datum-json price seller id` where seller is $(cat testnet/wallets/_your_wallet_name.pkh, id is the token you want to sell (unsig00005 -> id = 00005) 
* run `cardano-cli address build --payment-script-file market-testnet.plutus $Testnet > market-testnet.addr`
* run `./offerToken.sh id your_wallet_name` 

#### buy NFT 

* run `./buyToken.sh id your_wallet_name price` 

#### close the Sale 

* run `./closeSale.sh id your_wallet_name`

#### update price 

* run `mv datum-_id_.json datum-_id_-old.json` (testnet folder)
* run `cabal run datum-json price seller id` where price is the new price (unsigs-market-plutus folder)
* run `./updateSale.sh id your_wallet_name`(testnet folder)

## Mainnet

* you can find the mainnet contract address in /mainnet 
* for interaction you should use the frontend else you could lose your tokens forever.
* for testing you can use the testnet without real value

